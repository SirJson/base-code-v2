﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PixTech
{
	public class StringListFile
	{
		public List<string> Values = new List<string>();

		public StringListFile(string filePath)
		{
			filePath = Engine.Instance.Resources.RootDirectory + "/" + filePath;
			StreamReader streamReader = new StreamReader(filePath);
			string configContentStr = streamReader.ReadToEnd();
			streamReader.Close();
			foreach (string line in configContentStr.Split(new string[] { Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries))
			{
				if (!line.StartsWith("#") && line.Length > 0)
				{
					Values.Add(line);
				}
			}
		}
	}
}
