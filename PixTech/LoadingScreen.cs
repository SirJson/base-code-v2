﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PixTech
{
	public class LoadingScreen
	{
		public bool IsLoading { get; protected set; }
		public virtual void Load(string resources) { }
		public virtual void Draw() { }
		public virtual void Update() { }

		protected void FireOnLoadingFinished()
		{
			EventArgs e = new EventArgs();
			EventHandler<EventArgs> myEvent = OnLoadingFinished;
			if (myEvent != null)
			{
				myEvent(this, e);
			}
		}

		public event EventHandler<EventArgs> OnLoadingFinished;
	}
}
