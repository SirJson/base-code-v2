﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace PixTech
{
	public enum Shape
	{
		Rectangle,
		Ellipse,
		Polygon,
		Polyline
	}

	public class Layer
	{
		public uint[] Data;
		public int Width;
		public int Height;

		public uint GetTile(int x, int y)
		{
			if (x < 0 || y < 0 || x >= Width || y >= Height)
			{
				return 0;
			}
			return Data[y * Width + x];
		}
	}

	public class Tileset
	{
		public uint FirstGID, LastGID;
		public int TileWidth, TileHeight;
		public int Spacing;
		public int TilesPerRow;
		public Texture2D Texture;

		public Rectangle GetRectangleForTile(uint tileID)
		{
			uint localID = tileID - FirstGID; // Die ID innerhalb der Tileset Textur
			int x = (localID % TilesPerRow).ToInt32();
			int y = (localID / TilesPerRow).ToInt32();
			return new Rectangle(x * TileWidth, y * TileHeight, TileWidth, TileHeight);
		}
	}

	public class TileObject
	{
		public Dictionary<string, string> Properties;
		public Shape Shape;
		public string Name;
		public int X, Y;
		public int Width, Height;
	}

	public class TMXFile
	{
		public List<Layer> Layers = new List<Layer>();
		public List<Tileset> Tilesets = new List<Tileset>();
		public List<TileObject> TileObjects = new List<TileObject>();
		public Dictionary<string, string> Properties;
		public int Width;
		public int Height;
		public int TileWidth;
		public int TileHeight;

		private ResourceManager resources;

		public TMXFile(string filePath)
		{
			filePath = Engine.Instance.Resources.RootDirectory + "/" + filePath;
			resources = Engine.Instance.Resources;

			this.Parse(filePath);
		}

		public Dictionary<string, string> ParseProperties(XmlNode node)
		{
			XmlNode currentProperty = node.FirstChildElement("property");
			Dictionary<string, string> output = new Dictionary<string, string>();
			do
			{
				output[currentProperty.Attributes["name"].Value] = currentProperty.Attributes["value"].Value;
				currentProperty = node.NextSiblingElement("property");
			}
			while (currentProperty != null);
			return output;
		}

		public void ParseTileset(XmlNode node)
		{
			Tileset tileset = new Tileset();
			tileset.FirstGID = node.Attributes["firstgid"].Value.ToUInt32();
			tileset.LastGID = Int32.MaxValue;
			tileset.TileWidth = node.Attributes["tilewidth"].Value.ToInt32();
			tileset.TileHeight = node.Attributes["tileheight"].Value.ToInt32();
			if (node.Attributes["spacing"] != null) tileset.Spacing = node.Attributes["spacing"].Value.ToInt32();
			tileset.Texture = resources.Load<Texture2D>(node.FirstChildElement("image").Attributes["source"].Value);
			tileset.TilesPerRow = tileset.Texture.Width / tileset.TileWidth;
			if (Tilesets.Count > 0)
				Tilesets[Tilesets.Count - 1].LastGID = tileset.FirstGID - 1;
			Tilesets.Add(tileset);
		}

		public void ParseLayer(XmlNode node)
		{
			Layer layer = new Layer();
			XmlNode dataNode = node.FirstChildElement("data");
			if (dataNode.Attributes["encoding"].Value != "base64")
				throw new FormatException();
			byte[] data = Convert.FromBase64String(dataNode.FirstChild.Value);
			layer.Data = new uint[data.Length / 4];
			layer.Width = this.Width;
			layer.Height = this.Height;
			int index = 0;
			for (uint i = 0; i < data.Length; i += 4)
			{
				layer.Data[index] = (data[i + 0] |
								 data[i + 1] << 8	|
								 data[i + 2] << 16	|
								 data[i + 3] << 24).ToUInt32();
				index++;
			}
			Layers.Add(layer);
		}

		public void ParseObjectGroup(XmlNode node)
		{
			XmlNode objectNode = node.FirstChildElement("object");
			do
			{
				TileObject objectDef = new TileObject();

				if (objectNode.Attributes["name"] != null) objectDef.Name = objectNode.Attributes["name"].Value;
				if (objectNode.Attributes["type"] != null) objectDef.Name = objectNode.Attributes["type"].Value;
				if (objectNode.Attributes["width"] != null) objectDef.Width = objectNode.Attributes["width"].Value.ToInt32();
				if (objectNode.Attributes["height"] != null) objectDef.Height = objectNode.Attributes["height"].Value.ToInt32();
				objectDef.X = objectNode.Attributes["x"].Value.ToInt32();
				objectDef.Y = objectNode.Attributes["y"].Value.ToInt32();

				XmlNode objectProperties = objectNode.FirstChildElement("properties");
				if (objectProperties != null)
					objectDef.Properties = ParseProperties(objectProperties);

				objectDef.Shape = Shape.Rectangle;
				if (objectNode.FirstChildElement("ellipse") != null)
					objectDef.Shape = Shape.Ellipse;
				TileObjects.Add(objectDef);
				objectNode = objectNode.NextSiblingElement("object");
			}
			while (objectNode != null);
		}

		public void Parse(string filePath)
		{
			XmlDocument doc = new XmlDocument();
			doc.Load(new FileStream(filePath, FileMode.Open));
			XmlNode root = doc.DocumentElement;
			Width = root.Attributes["width"].Value.ToInt32();
			Height = root.Attributes["height"].Value.ToInt32();
			TileWidth = root.Attributes["tilewidth"].Value.ToInt32();
			TileHeight = root.Attributes["tileheight"].Value.ToInt32();

			if (root.HasChildNodes)
			{
				foreach (XmlNode child in root.ChildNodes)
				{
					switch (child.Name)
					{
						case "properties":
							this.Properties = this.ParseProperties(child);
							break;
						case "tileset":
							this.ParseTileset(child);
							break;
						case "layer":
							this.ParseLayer(child);
							break;
						case "objectgroup":
							this.ParseObjectGroup(child);
							break;
					}
				}
			}
		}

	}
}
