﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PixTech.GUI
{
	public class Container : Element
	{
		public List<Element> Children = new List<Element>();

		public override void Update()
		{
			foreach (var child in Children)
				child.Update();
		}

		public override void Draw(Vector2 offset, SpriteBatch batch)
		{
			foreach (var child in Children)
				child.Draw(offset + Position, batch);
		}

		public void Clear()
		{
			Children.Clear();
		}
	}
}
