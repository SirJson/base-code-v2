﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PixTech.GUI
{
	public class Element
	{
		public virtual void Update() {}
		public virtual void Draw(Vector2 offset, SpriteBatch batch) {}
		public virtual void ParseProperties() {
			if (Properties.ContainsKey("x") && Properties.ContainsKey("y"))
				this.Position = new Vector2(Properties["x"].ToInt32(), Properties["y"].ToInt32());
			if (Properties.ContainsKey("id"))
				this.ID = Properties["id"];
		}

		public Vector2 Position;
		public string ID;
		public Dictionary<string, string> Properties = new Dictionary<string, string>();
	}
}
