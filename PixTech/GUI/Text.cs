﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PixTech.GUI
{
	public class Text : Element
	{
		public string Data;
		public SpriteFont Font;

		public Text(string text, SpriteFont font)
		{
			Data = text.Replace("\t", "").TrimStart('\r', '\n');
			Font = font;
		}

		public override void ParseProperties()
		{
			base.ParseProperties();
			
		}

		public override void Draw(Vector2 offset, SpriteBatch batch)
		{
			batch.DrawString(Font, Data, offset, Color.White);
		}
	}
}
