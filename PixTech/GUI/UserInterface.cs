﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace PixTech.GUI
{
	public class UserInterface
	{
		private SpriteBatch uiBatch;
		private Container document = new Container();
		private Dictionary<string, Element> namedElements = new Dictionary<string, Element>();

		public SpriteFont DefaultFont;

		public UserInterface(GraphicsDevice device)
		{
			uiBatch = new SpriteBatch(device);
		}

		public void Update()
		{
			document.Update();
		}

		public void Draw()
		{
			uiBatch.Begin();
			document.Draw(Vector2.Zero,uiBatch);
			uiBatch.End();
		}

		public Element GetElementById(string id)
		{
			return namedElements[id];
		}

		public void Clear()
		{
			document.Clear();
			namedElements.Clear();
		}

		public void ReadNode(Container element, XmlNode node)
		{
			switch (node.NodeType)
			{
				case XmlNodeType.Element:
					switch (node.Name)
					{
						case "group":
							Container div = new Container();
							if (node.Attributes != null)
							{
								foreach (XmlAttribute attribute in node.Attributes)
								{
									if (attribute.Name == "id")
									{
										div.ID = attribute.Value;
										namedElements[attribute.Value] = div;
									}
									div.Properties.Add(attribute.Name, attribute.Value);
								}
								div.ParseProperties();
							}
							element.Children.Add(div);
							if (node.HasChildNodes)
							{
								foreach (XmlNode child in node.ChildNodes)
								{
									ReadNode(div,child);
								}
							}
							break;
					}
					break;
				case XmlNodeType.Text:
					element.Children.Add(new Text(node.Value, DefaultFont));
					break;
			}
		}

		public void Load(string file)
		{
			file = Engine.Instance.Content.RootDirectory + "/" + file;
			document.Clear();
			XmlDocument doc = new XmlDocument();
			doc.Load(new FileStream(file, FileMode.Open));
			if (doc.DocumentElement.HasChildNodes)
			{
				foreach (XmlNode child in doc.DocumentElement.ChildNodes)
				{
					ReadNode(document, child);
				}
			}
		}
	}
}
