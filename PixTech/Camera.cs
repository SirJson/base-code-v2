﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PixTech
{
    public class Camera
    {
        private Vector2 _pos = Vector2.Zero;

        public Matrix Transform
        {
            protected set;
            get;
        }

        public Vector2 Position
        {
            get
            {
                return _pos;
            }
            set
            {
                _pos = value;
                Transform = Matrix.CreateTranslation(new Vector3(_pos,0));
            }
        }

        public Camera()
        {
            Position = new Vector2(0, 0);
        }
    }
}
