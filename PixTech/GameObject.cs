﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PixTech
{
	public class GameObject
	{
		public Vector2 Position = Vector2.Zero;
		public float Width { get; protected set; }
		public float Height { get; protected set; }
		public bool Collidable = false;

		private List<Component> components = new List<Component>();

		public void Draw(SpriteBatch batch)
		{
			foreach (var component in components)
			{
				component.Draw(batch);
			}
		}

		public void Update(GameTime time)
		{
			foreach (var component in components)
			{
				component.Update(time.ElapsedGameTime.Milliseconds);
			}
		}

		public virtual void OnCollide(GameObject other) 
		{
		}

		public T AddComponent<T>() where T : Component, new()
		{
			T component = new T();
			component.Parent = this;
			components.Add(component);
			return component;
		}
	}
}
