﻿using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PixTech
{
	public class SceneGraph
	{
		private SpriteBatch spriteBatch;
		private List<GameObject> objects = new List<GameObject>();
		private Dictionary<string, GameObject> namedObjects = new Dictionary<string, GameObject>();

		public SceneGraph(GraphicsDevice device)
		{
			spriteBatch = new SpriteBatch(device);
		}

		public GameObject CreateObject()
		{
			GameObject obj = new GameObject();
			objects.Add(obj);
			return obj;
		}

		public GameObject CreateObject(string name)
		{
			GameObject obj = new GameObject();
			objects.Add(obj);
			namedObjects[name] = obj;
			return obj;
		}

		public void Remove(GameObject obj)
		{
			objects.Remove(obj);
		}
		
		public void Remove(string name)
		{
			objects.Remove(namedObjects[name]);
			namedObjects.Remove(name);
		}

		public void Clear()
		{
			objects.Clear();
			namedObjects.Clear();
		}

		public void Draw()
		{
			spriteBatch.Begin();
			foreach (GameObject obj in objects)
			{
				obj.Draw(spriteBatch);
			}
			spriteBatch.End();
		}
	}
}
