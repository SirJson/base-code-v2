﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PixTech
{
	class UIElement
	{
		public virtual void Draw()
		{
			
		}

		public virtual void Update()
		{

		}
	}
}
