﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PixTech
{
	public interface IGameState
	{
		void Init();
		void Update();
		void Draw();
	}
}
