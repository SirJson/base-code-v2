﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace PixTech
{
	public static class Utility
	{
		public static Point ToPoint(this Vector2 position)
		{
			return new Point((int)position.X, (int)position.Y);
		}

		public static Vector2 ToVector2(this Point point)
		{
			return new Vector2(point.X, point.Y);
		}

		public static int ToInt32(this String str)
		{
			return Convert.ToInt32(str);
		}

		public static int ToInt32(this long val)
		{
			return Convert.ToInt32(val);
		}

		public static uint ToUInt32(this String str)
		{
			return Convert.ToUInt32(str);
		}

		public static uint ToUInt32(this int val)
		{
			return Convert.ToUInt32(val);
		}

		public static XmlNode FirstChildElement(this XmlNode xmlNode, string name)
		{
			foreach (XmlNode node in xmlNode.ChildNodes)
			{
				if (node.Name == name)
					return node;
			}
			return null;
		}

		public static XmlNode NextSiblingElement(this XmlNode xmlNode, string name)
		{
			XmlNode sibling = xmlNode.NextSibling;
			while (sibling != null)
			{
				if (sibling.Name == name)
					return sibling;
				else
					sibling = sibling.NextSibling;
			}
			return null;
		}
	}
}
