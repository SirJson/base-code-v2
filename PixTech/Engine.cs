﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using PixTech.GUI;

namespace PixTech
{
	public class Engine : Game
	{
		private static Engine instance;

		public GraphicsDeviceManager DeviceManager { get; private set; }
		public SceneGraph Scene { get; private set; }
		public GameStateManager GameState { get; private set; }
		public ResourceManager Resources { get; private set; }
		public UserInterface GUI { get; private set; }
		public TileMap Map { get; private set; }

		private Engine() {
			DeviceManager = new GraphicsDeviceManager(this);
			DeviceManager.PreferredBackBufferWidth = 800;
			DeviceManager.PreferredBackBufferHeight = 600;
			Content.RootDirectory = "data";
		}

		public static Engine Instance
		{
			get 
			{
				if (instance == null) {
					instance = new Engine();
				}
				return instance;
			}
		}

		public void Create(IGameState gameState, LoadingScreen loadingScreen)
		{
			LogSystem.Instance.Init();
			Resources = new ResourceManager(this.Services, "data");
			Content = new ResourceManager(this.Services, "data");
			Scene = new SceneGraph(GraphicsDevice);
			GUI = new UserInterface(GraphicsDevice);
			Map = new TileMap(GraphicsDevice);
			GameState = new GameStateManager(loadingScreen);
			GameState.KnownStates["default"] = gameState;
			GameState.Set("default");
			this.Run();
		}

		protected override void Initialize()
		{
			base.Initialize();
		}

		protected override void LoadContent()
		{
			base.LoadContent();
		}

		protected override void Update(GameTime gameTime)
		{
			//this.Scene.Update();
			this.GameState.Update();
			this.GUI.Update();
			base.Update(gameTime);
		}

		protected override void Draw(GameTime gameTime)
		{
			GraphicsDevice.Clear(Color.CornflowerBlue);
			this.Map.Draw();
			this.Scene.Draw();
			this.GameState.Draw();
			this.GUI.Draw();
			base.Draw(gameTime);
		}
	}
}
