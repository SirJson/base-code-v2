﻿using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PixTech
{
	public class Component
	{
		public virtual void Draw(SpriteBatch batch) { }
		public virtual void Update(float deltaTime) { }

		public GameObject Parent;
	}
}
