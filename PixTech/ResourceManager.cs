﻿using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace PixTech
{
	public class ResourceManager : ContentManager
	{
		static object loadLock = new object();
		List<Thread> runningThreads = new List<Thread>();

		public bool IsLoading
		{
			get
			{
				foreach (Thread thread in runningThreads)
				{
					if (thread.IsAlive)
						return true;
				}
				return false;
			}
		}

		public ResourceManager(IServiceProvider serviceProvider)
			: base(serviceProvider)
		{
		}

		public ResourceManager(IServiceProvider serviceProvider, string rootDirectory)
			: base(serviceProvider, rootDirectory)
		{
		}

		public T Get<T>(string name)
		{
			return (T)LoadedAssets[name];
		}

		public void LoadAsync<T>(string assetName)
		{
			ThreadStart definition = new ThreadStart(() => SimpleLoad<T>(assetName));
			Thread thread = new Thread(definition);
			runningThreads.Add(thread);
			thread.Start();
		}

		public override T Load<T>(string assetName)
		{
			lock (loadLock)
			{
				return base.Load<T>(assetName);
			}
		}

		public void SimpleLoad<T>(string assetName)
		{
			lock (loadLock)
			{
				base.Load<T>(assetName);
			}
		}
	}
}
