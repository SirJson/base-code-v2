﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PixTech
{
	public class GameStateManager
	{
		public Dictionary<string, IGameState> KnownStates = new Dictionary<string, IGameState>();

		private LoadingScreen loadingScreen = null;
		private IGameState lastState = null;
		private IGameState runningState = null;
		private bool runningStateInitialized = false;

		public GameStateManager(LoadingScreen loadingScreen)
		{
			this.loadingScreen = loadingScreen;
			loadingScreen.OnLoadingFinished += InitRunningState;
		}

		public void Set(string name, string resourceDefinition = "")
		{
			runningStateInitialized = false;
			lastState = runningState;
			runningState = KnownStates[name];
			if (resourceDefinition.Length > 0)
			{
				loadingScreen.Load(resourceDefinition);
			}
			else
			{
				this.RunState();
			}
		}

		public void InitRunningState(object sender, EventArgs e)
		{
			this.RunState();
		}

		public void RunState()
		{
			runningStateInitialized = true;
			runningState.Init();
		}

		public void Restart()
		{
			//world.CleanupAll();
			runningState.Init();
		}

		public void Update()
		{
			if (!runningStateInitialized) loadingScreen.Update();
			else if (runningState != null) runningState.Update();
		}

		public void Draw()
		{
			if (!runningStateInitialized) loadingScreen.Draw();
			else if (runningState != null) runningState.Draw();
		}
	}
}
