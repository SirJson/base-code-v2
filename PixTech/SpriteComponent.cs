﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PixTech
{
	public class SpriteComponent : Component
	{
		public Texture2D Texture;

		public override void Draw(SpriteBatch batch)
		{
			if (Texture != null)
				batch.Draw(Texture,this.Parent.Position,Color.White);
		}

		public override void Update(float deltaTime)
		{

		}
	}
}
