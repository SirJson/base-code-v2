﻿using PixTech;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PixTech
{
	public enum LogLevel
	{
		Info,
		Warning,
		Error
	}

	class LogSystem
	{
		private LogSystem() {}
		public static readonly LogSystem Instance = new LogSystem();
		private string path;

		public void Init()
		{
			path = "game.log";
			if (File.Exists(path))
				File.Delete(path);
		}

		public void Write(string data)
		{
			StreamWriter logFile = new StreamWriter(path,true);
			string line = String.Format("{0} - {1}", DateTime.Now.ToLongTimeString(), data);
			Console.WriteLine(line);
			logFile.WriteLine(line);
			logFile.Close();
		}
	}
}
