﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PixTech
{
	public class Config
	{
		public Dictionary<string, string> Values = new Dictionary<string, string>();

		public Config(string filePath)
		{
			LogSystem.Instance.Write("Read Config...");
			filePath = Engine.Instance.Resources.RootDirectory + "/" + filePath;
			StreamReader streamReader = new StreamReader(filePath);
			string configContentStr = streamReader.ReadToEnd();
			streamReader.Close();
			foreach (string line in configContentStr.Split(new string[] { Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries))
			{
				if (!line.StartsWith("#") && line.Length > 0)
				{
					string[] keyValPair = line.Split('=');
					keyValPair[0] = keyValPair[0].Trim();
					keyValPair[1] = keyValPair[1].Trim();
					Values[keyValPair[0]] = keyValPair[1];
				}
			}
			LogSystem.Instance.Write("Config loaded!");
		}

		public T Get<T>(string index)
		{
			return (T)Convert.ChangeType(Values[index], typeof(T));
		}

		public void Save()
		{
		}
	}
}
