﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PixTech
{
	public class TileMap
	{
		SpriteBatch batch;
		TMXFile loadedFile;

		public TileMap(GraphicsDevice device)
		{
			batch = new SpriteBatch(device);
		}

		public void Load(TMXFile file)
		{
			loadedFile = file;
		}

		public void Draw()
		{
			if (loadedFile == null)
				return;
			batch.Begin();
			foreach (Layer layer in loadedFile.Layers)
			{
				for (int x = 0; x <= loadedFile.Width; x++)
				{
					for (int y = 0; y <= loadedFile.Height; y++)
					{
						uint tileGID = layer.GetTile(x, y);
						if (tileGID == 0) continue;
						Tileset tileset = null;
						foreach (Tileset set in loadedFile.Tilesets)
						{
							if (tileGID >= set.FirstGID && tileGID <= set.LastGID)
								tileset = set;
						}
						if (tileset == null) continue;

						Vector2 position = new Vector2(x * loadedFile.TileWidth,y * loadedFile.TileHeight);

						batch.Draw(tileset.Texture, position, tileset.GetRectangleForTile(tileGID), Color.White);
					}
				}
			}
			batch.End();
		}
	}
}
